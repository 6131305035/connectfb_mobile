import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View , Image} from 'react-native';


const dfstudent = [
  {
      "first_name": "Emili",
      "id": 1,
      "img": "http://dummyimage.com/250x231.jpg/dddddd/000000",
      "last_name": "Stritton"
  },
  {
      "first_name": "Gwynne",
      "id": 2,
      "img": "http://dummyimage.com/224x154.jpg/dddddd/000000",
      "last_name": "Crowley"
  },
  {
      "first_name": "Felizio",
      "id": 3,
      "img": "http://dummyimage.com/221x194.jpg/ff4444/ffffff",
      "last_name": "O'Donohue"
  },
  {
      "first_name": "Erasmus",
      "id": 4,
      "img": "http://dummyimage.com/110x126.jpg/5fa2dd/ffffff",
      "last_name": "Redington"
  },
  {
      "first_name": "Kai",
      "id": 5,
      "img": "http://dummyimage.com/187x203.jpg/dddddd/000000",
      "last_name": "Buesden"
  },
  {
      "first_name": "Karalee",
      "id": 6,
      "img": "http://dummyimage.com/154x129.jpg/5fa2dd/ffffff",
      "last_name": "Bonhill"
  },
  {
      "first_name": "Archibald",
      "id": 7,
      "img": "http://dummyimage.com/114x237.jpg/dddddd/000000",
      "last_name": "Skipsea"
  },
  {
      "first_name": "Latisha",
      "id": 8,
      "img": "http://dummyimage.com/126x110.jpg/dddddd/000000",
      "last_name": "Huge"
  },
  {
      "first_name": "Morry",
      "id": 9,
      "img": "http://dummyimage.com/156x124.jpg/cc0000/ffffff",
      "last_name": "Fouch"
  },
  {
      "first_name": "Gus",
      "id": 10,
      "img": "http://dummyimage.com/157x157.jpg/ff4444/ffffff",
      "last_name": "Franey"
  },
  {
      "first_name": "Etan",
      "id": 11,
      "img": "http://dummyimage.com/127x123.jpg/dddddd/000000",
      "last_name": "Janeway"
  },
  {
      "first_name": "Darnall",
      "id": 12,
      "img": "http://dummyimage.com/159x124.jpg/5fa2dd/ffffff",
      "last_name": "Richardon"
  },
  {
      "first_name": "Travis",
      "id": 13,
      "img": "http://dummyimage.com/173x248.jpg/ff4444/ffffff",
      "last_name": "Parcells"
  },
  {
      "first_name": "Lorin",
      "id": 14,
      "img": "http://dummyimage.com/155x204.jpg/dddddd/000000",
      "last_name": "Holligan"
  },
  {
      "first_name": "Rayshell",
      "id": 15,
      "img": "http://dummyimage.com/147x107.jpg/ff4444/ffffff",
      "last_name": "Hoodlass"
  },
  {
      "first_name": "Jasmina",
      "id": 16,
      "img": "http://dummyimage.com/112x184.jpg/5fa2dd/ffffff",
      "last_name": "Staterfield"
  },
  {
      "first_name": "Killy",
      "id": 17,
      "img": "http://dummyimage.com/118x123.jpg/5fa2dd/ffffff",
      "last_name": "Lougheid"
  },
  {
      "first_name": "Nadeen",
      "id": 18,
      "img": "http://dummyimage.com/241x138.jpg/cc0000/ffffff",
      "last_name": "Waddup"
  },
  {
      "first_name": "Micky",
      "id": 19,
      "img": "http://dummyimage.com/248x143.jpg/5fa2dd/ffffff",
      "last_name": "Lascell"
  },
  {
      "first_name": "La verne",
      "id": 20,
      "img": "http://dummyimage.com/168x145.jpg/ff4444/ffffff",
      "last_name": "Loxston"
  },
  {
      "first_name": "Adiana",
      "id": 21,
      "img": "http://dummyimage.com/229x167.jpg/cc0000/ffffff",
      "last_name": "Duligal"
  },
  {
      "first_name": "Quintina",
      "id": 22,
      "img": "http://dummyimage.com/114x120.jpg/5fa2dd/ffffff",
      "last_name": "Buzzing"
  },
  {
      "first_name": "Niccolo",
      "id": 23,
      "img": "http://dummyimage.com/102x243.jpg/cc0000/ffffff",
      "last_name": "Leeman"
  },
  {
      "first_name": "Arty",
      "id": 24,
      "img": "http://dummyimage.com/225x129.jpg/cc0000/ffffff",
      "last_name": "Lowsely"
  },
  {
      "first_name": "Seka",
      "id": 25,
      "img": "http://dummyimage.com/168x172.jpg/cc0000/ffffff",
      "last_name": "Jeste"
  },
  {
      "first_name": "Barny",
      "id": 26,
      "img": "http://dummyimage.com/218x178.jpg/dddddd/000000",
      "last_name": "Fysh"
  },
  {
      "first_name": "Liv",
      "id": 27,
      "img": "http://dummyimage.com/151x242.jpg/cc0000/ffffff",
      "last_name": "Portam"
  },
  {
      "first_name": "Kassandra",
      "id": 28,
      "img": "http://dummyimage.com/115x197.jpg/dddddd/000000",
      "last_name": "Ardy"
  },
  {
      "first_name": "Rozina",
      "id": 29,
      "img": "http://dummyimage.com/138x135.jpg/cc0000/ffffff",
      "last_name": "Crellin"
  },
  {
      "first_name": "Melli",
      "id": 30,
      "img": "http://dummyimage.com/238x202.jpg/cc0000/ffffff",
      "last_name": "Cohalan"
  },
  {
      "first_name": "Mikol",
      "id": 31,
      "img": "http://dummyimage.com/167x154.jpg/dddddd/000000",
      "last_name": "Evelyn"
  },
  {
      "first_name": "Dannie",
      "id": 32,
      "img": "http://dummyimage.com/199x233.jpg/5fa2dd/ffffff",
      "last_name": "Ligoe"
  },
  {
      "first_name": "Evy",
      "id": 33,
      "img": "http://dummyimage.com/157x224.jpg/cc0000/ffffff",
      "last_name": "Spalton"
  },
  {
      "first_name": "Timothee",
      "id": 34,
      "img": "http://dummyimage.com/117x125.jpg/ff4444/ffffff",
      "last_name": "Cranage"
  },
  {
      "first_name": "Averil",
      "id": 35,
      "img": "http://dummyimage.com/202x159.jpg/5fa2dd/ffffff",
      "last_name": "Bannell"
  },
  {
      "first_name": "Fanya",
      "id": 36,
      "img": "http://dummyimage.com/163x205.jpg/5fa2dd/ffffff",
      "last_name": "McDermot"
  },
  {
      "first_name": "Luis",
      "id": 37,
      "img": "http://dummyimage.com/222x103.jpg/cc0000/ffffff",
      "last_name": "Betje"
  },
  {
      "first_name": "Kary",
      "id": 38,
      "img": "http://dummyimage.com/213x234.jpg/5fa2dd/ffffff",
      "last_name": "Latter"
  },
  {
      "first_name": "Zelma",
      "id": 39,
      "img": "http://dummyimage.com/227x109.jpg/cc0000/ffffff",
      "last_name": "Dooney"
  },
  {
      "first_name": "Caryl",
      "id": 40,
      "img": "http://dummyimage.com/105x205.jpg/dddddd/000000",
      "last_name": "Bugge"
  },
  {
      "first_name": "Trumann",
      "id": 41,
      "img": "http://dummyimage.com/106x213.jpg/5fa2dd/ffffff",
      "last_name": "Le Houx"
  },
  {
      "first_name": "Dame",
      "id": 42,
      "img": "http://dummyimage.com/191x226.jpg/ff4444/ffffff",
      "last_name": "Eddisford"
  },
  {
      "first_name": "Gail",
      "id": 43,
      "img": "http://dummyimage.com/229x227.jpg/dddddd/000000",
      "last_name": "Wherton"
  },
  {
      "first_name": "Lucinda",
      "id": 44,
      "img": "http://dummyimage.com/215x139.jpg/5fa2dd/ffffff",
      "last_name": "Remer"
  },
  {
      "first_name": "Suellen",
      "id": 45,
      "img": "http://dummyimage.com/220x195.jpg/cc0000/ffffff",
      "last_name": "Wyant"
  },
  {
      "first_name": "Hephzibah",
      "id": 46,
      "img": "http://dummyimage.com/117x181.jpg/cc0000/ffffff",
      "last_name": "Chettle"
  },
  {
      "first_name": "Elka",
      "id": 47,
      "img": "http://dummyimage.com/143x216.jpg/dddddd/000000",
      "last_name": "Perle"
  },
  {
      "first_name": "Ashley",
      "id": 48,
      "img": "http://dummyimage.com/102x221.jpg/dddddd/000000",
      "last_name": "Lickorish"
  },
  {
      "first_name": "Hewie",
      "id": 49,
      "img": "http://dummyimage.com/111x233.jpg/ff4444/ffffff",
      "last_name": "Eglington"
  },
  {
      "first_name": "Dinah",
      "id": 50,
      "img": "http://dummyimage.com/236x231.jpg/dddddd/000000",
      "last_name": "Jenken"
  },
  {
      "first_name": "Arlyne",
      "id": 51,
      "img": "http://dummyimage.com/123x224.jpg/cc0000/ffffff",
      "last_name": "Peirpoint"
  },
  {
      "first_name": "Leslie",
      "id": 52,
      "img": "http://dummyimage.com/105x175.jpg/5fa2dd/ffffff",
      "last_name": "Inett"
  },
  {
      "first_name": "Jonah",
      "id": 53,
      "img": "http://dummyimage.com/218x110.jpg/cc0000/ffffff",
      "last_name": "Husband"
  },
  {
      "first_name": "Russ",
      "id": 54,
      "img": "http://dummyimage.com/219x162.jpg/5fa2dd/ffffff",
      "last_name": "Shimwall"
  },
  {
      "first_name": "Devlen",
      "id": 55,
      "img": "http://dummyimage.com/163x150.jpg/ff4444/ffffff",
      "last_name": "Winspeare"
  },
  {
      "first_name": "Grannie",
      "id": 56,
      "img": "http://dummyimage.com/214x170.jpg/ff4444/ffffff",
      "last_name": "Ditty"
  },
  {
      "first_name": "Dalli",
      "id": 57,
      "img": "http://dummyimage.com/139x150.jpg/dddddd/000000",
      "last_name": "Hallwood"
  },
  {
      "first_name": "Vin",
      "id": 58,
      "img": "http://dummyimage.com/174x239.jpg/ff4444/ffffff",
      "last_name": "Boaler"
  },
  {
      "first_name": "Jonell",
      "id": 59,
      "img": "http://dummyimage.com/169x193.jpg/5fa2dd/ffffff",
      "last_name": "Skellern"
  },
  {
      "first_name": "Nicole",
      "id": 60,
      "img": "http://dummyimage.com/145x161.jpg/ff4444/ffffff",
      "last_name": "Allan"
  },
  {
      "first_name": "Rodolphe",
      "id": 61,
      "img": "http://dummyimage.com/159x160.jpg/dddddd/000000",
      "last_name": "Wathey"
  },
  {
      "first_name": "Morgun",
      "id": 62,
      "img": "http://dummyimage.com/227x234.jpg/dddddd/000000",
      "last_name": "Huikerby"
  },
  {
      "first_name": "Morna",
      "id": 63,
      "img": "http://dummyimage.com/247x106.jpg/5fa2dd/ffffff",
      "last_name": "O'Grogane"
  },
  {
      "first_name": "Crystie",
      "id": 64,
      "img": "http://dummyimage.com/196x169.jpg/dddddd/000000",
      "last_name": "Zanioletti"
  },
  {
      "first_name": "Kittie",
      "id": 65,
      "img": "http://dummyimage.com/208x207.jpg/dddddd/000000",
      "last_name": "Knuckles"
  },
  {
      "first_name": "Thaine",
      "id": 66,
      "img": "http://dummyimage.com/157x226.jpg/ff4444/ffffff",
      "last_name": "Povele"
  },
  {
      "first_name": "Viki",
      "id": 67,
      "img": "http://dummyimage.com/243x220.jpg/ff4444/ffffff",
      "last_name": "Denney"
  },
  {
      "first_name": "Brett",
      "id": 68,
      "img": "http://dummyimage.com/208x184.jpg/dddddd/000000",
      "last_name": "Hutcheons"
  },
  {
      "first_name": "Allen",
      "id": 69,
      "img": "http://dummyimage.com/110x216.jpg/ff4444/ffffff",
      "last_name": "Dunton"
  },
  {
      "first_name": "Smith",
      "id": 70,
      "img": "http://dummyimage.com/206x135.jpg/dddddd/000000",
      "last_name": "Chung"
  },
  {
      "first_name": "Eulalie",
      "id": 71,
      "img": "http://dummyimage.com/141x135.jpg/cc0000/ffffff",
      "last_name": "Simonazzi"
  },
  {
      "first_name": "Hunfredo",
      "id": 72,
      "img": "http://dummyimage.com/207x173.jpg/cc0000/ffffff",
      "last_name": "Hitter"
  },
  {
      "first_name": "Jacinta",
      "id": 73,
      "img": "http://dummyimage.com/108x197.jpg/dddddd/000000",
      "last_name": "Alessandrini"
  },
  {
      "first_name": "Alidia",
      "id": 74,
      "img": "http://dummyimage.com/174x210.jpg/5fa2dd/ffffff",
      "last_name": "Badini"
  },
  {
      "first_name": "Benedikt",
      "id": 75,
      "img": "http://dummyimage.com/213x155.jpg/cc0000/ffffff",
      "last_name": "Faldoe"
  },
  {
      "first_name": "Saba",
      "id": 76,
      "img": "http://dummyimage.com/226x195.jpg/5fa2dd/ffffff",
      "last_name": "Arnaldi"
  },
  {
      "first_name": "Ulrich",
      "id": 77,
      "img": "http://dummyimage.com/192x236.jpg/dddddd/000000",
      "last_name": "Lindberg"
  },
  {
      "first_name": "Michail",
      "id": 78,
      "img": "http://dummyimage.com/220x206.jpg/5fa2dd/ffffff",
      "last_name": "Broxton"
  },
  {
      "first_name": "Jillane",
      "id": 79,
      "img": "http://dummyimage.com/158x234.jpg/5fa2dd/ffffff",
      "last_name": "Lundie"
  },
  {
      "first_name": "Leora",
      "id": 80,
      "img": "http://dummyimage.com/211x213.jpg/5fa2dd/ffffff",
      "last_name": "Edworthie"
  },
  {
      "first_name": "Illa",
      "id": 81,
      "img": "http://dummyimage.com/118x150.jpg/ff4444/ffffff",
      "last_name": "Bows"
  },
  {
      "first_name": "Emory",
      "id": 82,
      "img": "http://dummyimage.com/215x151.jpg/5fa2dd/ffffff",
      "last_name": "Dendle"
  },
  {
      "first_name": "Christal",
      "id": 83,
      "img": "http://dummyimage.com/116x217.jpg/5fa2dd/ffffff",
      "last_name": "Trewin"
  },
  {
      "first_name": "Micheil",
      "id": 84,
      "img": "http://dummyimage.com/123x179.jpg/5fa2dd/ffffff",
      "last_name": "Riteley"
  },
  {
      "first_name": "Jodee",
      "id": 85,
      "img": "http://dummyimage.com/221x147.jpg/cc0000/ffffff",
      "last_name": "Dring"
  },
  {
      "first_name": "Anderson",
      "id": 86,
      "img": "http://dummyimage.com/118x154.jpg/dddddd/000000",
      "last_name": "De Coursey"
  },
  {
      "first_name": "Iver",
      "id": 87,
      "img": "http://dummyimage.com/208x189.jpg/cc0000/ffffff",
      "last_name": "Castagne"
  },
  {
      "first_name": "Corrie",
      "id": 88,
      "img": "http://dummyimage.com/210x213.jpg/dddddd/000000",
      "last_name": "Shillam"
  },
  {
      "first_name": "Trudy",
      "id": 89,
      "img": "http://dummyimage.com/180x126.jpg/ff4444/ffffff",
      "last_name": "Turban"
  },
  {
      "first_name": "Eadmund",
      "id": 90,
      "img": "http://dummyimage.com/196x150.jpg/cc0000/ffffff",
      "last_name": "Amorts"
  },
  {
      "first_name": "Fax",
      "id": 91,
      "img": "http://dummyimage.com/174x228.jpg/dddddd/000000",
      "last_name": "Milliere"
  },
  {
      "first_name": "Quincey",
      "id": 92,
      "img": "http://dummyimage.com/250x193.jpg/dddddd/000000",
      "last_name": "Purveys"
  },
  {
      "first_name": "Doralyn",
      "id": 93,
      "img": "http://dummyimage.com/112x206.jpg/cc0000/ffffff",
      "last_name": "Bywater"
  },
  {
      "first_name": "Hermie",
      "id": 94,
      "img": "http://dummyimage.com/106x227.jpg/ff4444/ffffff",
      "last_name": "Guye"
  },
  {
      "first_name": "Lawrence",
      "id": 95,
      "img": "http://dummyimage.com/134x169.jpg/ff4444/ffffff",
      "last_name": "Lawie"
  },
  {
      "first_name": "Carlin",
      "id": 96,
      "img": "http://dummyimage.com/234x164.jpg/ff4444/ffffff",
      "last_name": "Longstreeth"
  },
  {
      "first_name": "Penny",
      "id": 97,
      "img": "http://dummyimage.com/157x237.jpg/cc0000/ffffff",
      "last_name": "Duddin"
  },
  {
      "first_name": "Johny",
      "id": 98,
      "img": "http://dummyimage.com/236x161.jpg/ff4444/ffffff",
      "last_name": "De'Vere - Hunt"
  },
  {
      "first_name": "Archibold",
      "id": 99,
      "img": "http://dummyimage.com/134x154.jpg/cc0000/ffffff",
      "last_name": "Shovlin"
  },
  {
      "first_name": "Rafaelia",
      "id": 100,
      "img": "http://dummyimage.com/175x207.jpg/5fa2dd/ffffff",
      "last_name": "Cannop"
  }
]
;


export default class FlatListBasics extends Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true,students : dfstudent}
  } 

  async componentDidMount () {
    let fbstudent = await this.getStudentFromApi();
    this.setState({students : fbstudent});
  }

  getStudentFromApi = async() => {
    try {
      let response = await fetch('https://mobilelab-8613e.firebaseio.com/NewImageJSON/.json');
      let responseJson = await response.json();
      console.log(responseJson)
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.students}
          renderItem={({item}) => 
          
          <View style={{ marginTop: 2, flexDirection: 'row' }}>
            <Image source={{ uri: item.img }}
            style={{ width: 120, height: 120 }} />
 
              <View style={{ paddingLeft: 10 }}>
                  <Text >{item.first_name} {item.last_name}</Text>
                 
              </View> 
        </View>
          }
        />
      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    justifyContent : "center",
    
   flex: 1,
   
  },

  Arrange_1 : {
    justifyContent : 'space-around',
    flexDirection : "row",
    flex : 1,
    marginTop : 10
  },

  item: {
    justifyContent : "center",
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})
